import time
import random as rd
from rules import *
from models import *
from utils import Randomizer


class PandaCOO:
    """Class representing core functions of Pandarium world

    All setters may be accessed only with specific code,
    which should be defined when object is initialized

    Args:
        coo_id (int): COO identifier which allows to use setters and modify object state

    Attributes:
        _coo_id (int):                        COO identifier which allows to use setters and modify object state

        _base_score (int): Initial score.     Equal to score of attribute |1, 1, 1, 1| (lowest possible value)

        _alley_score_weights (list:`int`):    Score weights for genes in alley: D, R1, R2, R3

        _dance_matrix (list:`tuple`:`int`):   Mapping between attribute scores and dances.
                                              I-th tuple in list contains indexes of two attributes, which define score
                                              for I-th dance

        _alpha (float: [0, 1]):               Weight parameter for dance mapping. While dance depends on two attributes,
                                              its score equal:
                                                    1-st attribute score * alpha + 2-nd attribute score * (1 - alpha)

    """

    def __init__(
            self,
            coo_id,
            scale_parameter=100,
            base_score=1,
            min_weight=50,
            alpha=75,
            dance_matrix=(
                (3, 11),
                (8, 0),
                (1, 3),
                (11, 1),
                (0, 5),
                (6, 10),
                (2, 7),
                (9, 4),
                (10, 8),
                (4, 6),
                (5, 9),
                (7, 2)
            ),
            min_mutation_prob=20,
            precision=2147483648
    ):
        self._coo_id = coo_id
        self._precision = precision

        self._scale_parameter = scale_parameter
        self._base_score = base_score
        self._min_weight = min_weight
        self.set_alley_score_weights(coo_id)

        self._dance_matrix = dance_matrix
        self._alpha = alpha

        self.set_mutation_probability(min_mutation_prob)

        self._pandas = {}
        self._pandas_count = 0

    def onlyCOO(self, access_id):
        """Verify whether access_id is valid

        Args:
            access_id (int): code to verify
        Returns:
            Nothing if `access_id` is equal to `self._coo_id`
        Throws:
            `ValueError` if code is invalid
        """
        if self._coo_id != access_id:
            raise ValueError("Permission denied")

    def set_scale_parameter(self, access_id, scale_parameter):
        self.onlyCOO(access_id)
        self._scale_parameter = scale_parameter

    def set_base_score(self, access_id, base_score):
        self.onlyCOO(access_id)
        self._base_score = base_score

    def set_alley_score_weights(self, access_id, min_weight=None):
        """Calculate alley score genes

        Score weights for genes in alley are computed using value of lowest weight (R3).
        Highest value is always 1 (D), so `min_height` should be in range (0, 1).
        All other weights are computed as arithmetic progression of 4 elements with maximum in 1
        and minimum in `min_weight`.
        Then all values are normalized by dividing on `min_weight` (so weight for R3 will be equal to 1)

        Args:
            access_id (int): code to verify
            min_weight (float: (0, 1)): value of lowest weight
        """
        self.onlyCOO(access_id)

        if min_weight is None:
            min_weight = self._min_weight

        d = int((self._precision * (100 - min_weight) / 100) // 3)

        self._alley_score_weights = {
            "R3": self._precision,
            "R2": self._precision + int(d * 100 / min_weight),
            "R1": self._precision + int(d * 200 / min_weight),
            "D": self._precision * 100 // min_weight
        }

    def set_dance_matrix(self, access_id, alpha, dance_matrix):
        self.onlyCOO(access_id)

        if len(dance_matrix) != DANCE_COUNT:
            raise ValueError(
                "@param `cell_coordinates` should contain attributes mapping for {} dances".format(DANCE_COUNT)
            )

        self._dance_matrix = dance_matrix
        self._alpha = alpha

    def set_mutation_probability(self, min_mutation_prob):
        self._mutation_probabilities = {
            "R3": [],
            "R2": [],
            "R1": [],
            "D": []
        }

        d_alley = int((self._precision * (100 - self._min_weight) / 100) // 3)
        alley_weight = [self._min_weight * self._precision // 100 + d_alley * i for i in range(4)]
        alley_weight[3] = self._precision

        genes_count = MAX_GENE_VALUE - MIN_GENE_VALUE + 1
        d_mutation = int((100 - min_mutation_prob) * self._precision // 100 // (genes_count - 1))

        for i in range(genes_count):
            base_prob =int(min_mutation_prob * self._precision // 100) + d_mutation * i

            self._mutation_probabilities["R3"].append(int(base_prob * alley_weight[3] // self._precision))
            self._mutation_probabilities["R2"].append(int(base_prob * alley_weight[2] // self._precision))
            self._mutation_probabilities["R1"].append(int(base_prob * alley_weight[1] // self._precision))
            self._mutation_probabilities["D"].append(int(base_prob * alley_weight[0] // self._precision))

    def get_mutation_probability(self):
        return self._mutation_probabilities

    def generate_gen0(self):
        genotype = Genotype([Attribute() for i in range(ATTR_COUNT)])
        panda = Panda(
            id=self._pandas_count,
            genotype=genotype,
            generation=0
        )

        if panda.get_id() in self._pandas.keys():
            raise ValueError("Panda with id {} already exist".format(panda.get_id()))

        self._pandas[panda.get_id()] = panda
        self._pandas_count += 1

        return panda.get_id()

    def generate_random_panda(self):
        genotype = Genotype([Attribute(D=rd.randint(MIN_GENE_VALUE, MAX_GENE_VALUE),
                                       R1=rd.randint(MIN_GENE_VALUE, MAX_GENE_VALUE),
                                       R2=rd.randint(MIN_GENE_VALUE, MAX_GENE_VALUE),
                                       R3=rd.randint(MIN_GENE_VALUE, MAX_GENE_VALUE),) for i in range(ATTR_COUNT)])
        panda = Panda(
            id=self._pandas_count,
            genotype=genotype,
            generation=0
        )

        if panda.get_id() in self._pandas.keys():
            raise ValueError("Panda with id {} already exist".format(panda.get_id()))

        self._pandas[panda.get_id()] = panda
        self._pandas_count += 1

        return panda.get_id()

    def get_panda(self, panda_id):
        if panda_id not in self._pandas.keys():
            raise ValueError("Panda with id {} doesn't exist".format(panda_id))
        return self._pandas[panda_id]

    def get_pandas_attribute_score(self, panda_id, total=False):
        """Compute absolute score of panda

        Args:
            panda_id (`obj`: Panda): id of panda to score
            total (bool, optional): if False, returns array with score for each attribute separately,
                                    otherwise will return total score of panda (sum of attributes scores)
        """
        result = []
        panda = self.get_panda(panda_id)

        for attr in panda.get_genotype().get_attribute():
            attr_score = (attr.get_D() - 1) * self._alley_score_weights["D"] + \
                         (attr.get_R1() - 1) * self._alley_score_weights["R1"] + \
                         (attr.get_R2() - 1) * self._alley_score_weights["R2"] + \
                         (attr.get_R3() - 1) * self._alley_score_weights["R3"]

            attr_score *= self._scale_parameter
            attr_score //= self._precision
            attr_score += self._base_score * self._scale_parameter

            result.append(attr_score)

        if total:
            result = sum(result)

        return result

    def get_panda_dance_score(self, panda_id):
        """Compute score in dancing competition for panda

        Args:
            panda_id (`obj`: Panda): id of panda to score
        Returns:
            (`list`: float): score for each dance
        """
        result = []
        pandas_attribute_score = self.get_pandas_attribute_score(panda_id)

        for i in range(DANCE_COUNT):
            dance_score = pandas_attribute_score[self._dance_matrix[i][0]] * self._alpha // 100 + \
                          pandas_attribute_score[self._dance_matrix[i][1]] * (100 - self._alpha) // 100
            result.append(dance_score)

        return result

    def breed_pandas(self, matron_id, sire_id):
        matron = self.get_panda(matron_id)
        sire = self.get_panda(sire_id)

        child_attributes = []

        i = 0
        rd = Randomizer(-self._pandas_count)

        for m_attr, s_attr in zip(matron.get_genotype().get_attribute(), sire.get_genotype().get_attribute()):
            R3, R2, R1, D = m_attr.get_R3(), m_attr.get_R2(), m_attr.get_R1(), m_attr.get_D()

            delta_R3 = s_attr.get_R3() - m_attr.get_R3()
            mutation_bound = self._mutation_probabilities["R3"][abs(delta_R3)]

            if rd.random() < mutation_bound and R3 < MAX_GENE_VALUE:
                R3 += 1 if not delta_R3 else delta_R3 // abs(delta_R3)

            delta_R2 = s_attr.get_R2() - m_attr.get_R2()
            mutation_bound = self._mutation_probabilities["R2"][abs(delta_R2)]

            if rd.random() < mutation_bound and R2 < MAX_GENE_VALUE:
                R2 += 1 if not delta_R2 else delta_R2 // abs(delta_R2)

            delta_R1 = s_attr.get_R1() - m_attr.get_R1()
            mutation_bound = self._mutation_probabilities["R1"][abs(delta_R1)]

            if rd.random() < mutation_bound and R1 < MAX_GENE_VALUE:
                R1 += 1 if not delta_R1 else delta_R1 // abs(delta_R1)

            delta_D = s_attr.get_D() - m_attr.get_D()
            mutation_bound = self._mutation_probabilities["D"][abs(delta_D)]

            if rd.random() < mutation_bound and D < MAX_GENE_VALUE:
                D += 1 if not delta_D else delta_D // abs(delta_D)

            child_attributes.append(Attribute(D=D, R1=R1, R2=R2, R3=R3))

            i += 1

        child = Panda(
            id=self._pandas_count,
            genotype=Genotype(attributes=child_attributes),
            generation=max(matron.get_generation(), sire.get_generation()) + 1,
            matron_id=matron_id,
            sire_id=sire_id
        )

        self._pandas[child.get_id()] = child
        self._pandas_count += 1

        return child.get_id()



