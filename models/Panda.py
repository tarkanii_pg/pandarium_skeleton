class Panda:
    def __init__(self, id, genotype, generation, matron_id=None, sire_id=None):
        self._id = id
        self._genotype = genotype
        self._generation = generation
        self._matron_id = matron_id
        self._sire_id = sire_id

    def get_id(self):
        return self._id

    def get_genotype(self):
        return self._genotype

    def get_generation(self):
        return self._generation

    def get_matron_id(self):
        return self._matron_id

    def get_sire_id(self):
        return self._sire_id
