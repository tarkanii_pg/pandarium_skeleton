from rules import *


class Attribute:
    def __init__(self, D=MIN_GENE_VALUE, R1=MIN_GENE_VALUE, R2=MIN_GENE_VALUE, R3=MIN_GENE_VALUE):
        self._validate_gene(D)
        self._validate_gene(R1)
        self._validate_gene(R2)
        self._validate_gene(R3)

        self._D = D
        self._R1 = R1
        self._R2 = R2
        self._R3 = R3

    def _validate_gene(self, gene):
        if not MIN_GENE_VALUE <= gene <= MAX_GENE_VALUE:
            raise ValueError(
                "Gene must be in range [{}, {}], not {}".format(MIN_GENE_VALUE, MAX_GENE_VALUE, gene)
            )

    def get_D(self):
        return self._D

    def get_R1(self):
        return self._R1

    def get_R2(self):
        return self._R2

    def get_R3(self):
        return self._R3
