from rules import ATTR_COUNT
from .Attribute import Attribute


class Genotype:
    def __init__(self, attributes):
        if not (
            isinstance(attributes, list) and
            len(attributes) == ATTR_COUNT and
            all([isinstance(attribute, Attribute) for attribute in attributes])
        ):
            raise ValueError("@param `attributes` must be a list of {} objects of type `Attribute`".format(ATTR_COUNT))

        self._attributes = attributes

    def get_attribute(self, index=None):
        if index is None:
            return self._attributes

        return self._attributes[index]
