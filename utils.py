class Randomizer:
    IA = 16807
    IM = 2147483647
    AM = 1.0 / IM
    IQ = 127773
    IR = 2836
    NTAB = 32
    NDIV = 1 + (IM - 1) / NTAB
    EPS = 1.2e-7
    RNMX = 1.0 - EPS

    def __init__(self, seed):
        self.iy = 0
        self.iv = [0] * Randomizer.NTAB
        self.seed = seed

    def random(self):
        if self.seed <= 0 or not self.iy:
            self.seed = 1 if -self.seed < 1 else -self.seed

            for j in range(Randomizer.NTAB + 7, -1, -1):

                k = int(self.seed // Randomizer.IQ)
                self.seed = Randomizer.IA * (self.seed - k * Randomizer.IQ) - Randomizer.IR * k

                if self.seed < 0:
                    self.seed += Randomizer.IM
                if j < Randomizer.NTAB:
                    self.iv[j] = self.seed

            self.iy = self.iv[0]

        k = int(self.seed // Randomizer.IQ)
        self.seed = Randomizer.IA * (self.seed - k * Randomizer.IQ) - Randomizer.IR * k
        if self.seed < 0:
            self.seed += Randomizer.IM

        j = int(self.iy // Randomizer.NDIV)
        self.iy = self.iv[j]
        self.iv[j] = self.seed

        temp = self.iy
        return temp
