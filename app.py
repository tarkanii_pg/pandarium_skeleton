from models import *
from rules import *
from PandaCOO import PandaCOO
from utils import Randomizer


if __name__ == '__main__':
    # genotype = Genotype([Attribute(i + 1, i + 1, i + 1, i + 1) for i in range(ATTR_COUNT)])
    # panda = Panda(1, genotype, 0)
    #
    access_id = 123
    pandaCOO = PandaCOO(access_id)
    #
    # matron_id = pandaCOO.generate_random_panda()
    # print(matron_id)
    # print(pandaCOO.get_pandas_attribute_score(matron_id))
    # print(matron_id)
    # print(pandaCOO.get_panda_dance_score(matron_id))
    # print(matron_id)
    # print(pandaCOO.get_pandas_attribute_score(matron_id, total=True))
    # print(sum(pandaCOO.get_panda_dance_score(matron_id)))
    #
    matron_id = pandaCOO.generate_gen0()
    # print(matron_id)
    # print(pandaCOO.get_pandas_attribute_score(matron_id))
    # print(matron_id)
    # print(pandaCOO.get_panda_dance_score(matron_id))
    # print(matron_id)
    # print(pandaCOO.get_pandas_attribute_score(matron_id, total=True))
    # print(sum(pandaCOO.get_panda_dance_score(matron_id)))

    sire_id = pandaCOO.generate_random_panda()

    for i in range(1):
        child = pandaCOO.breed_pandas(matron_id, sire_id)
        print(pandaCOO.get_pandas_attribute_score(child))
        print(pandaCOO.get_panda_dance_score(child))
        print('--------------------------')

    print(pandaCOO.get_panda_dance_score(matron_id))
    print(pandaCOO.get_pandas_attribute_score(matron_id))


    # pandaCOO.set_base_score(access_id, 1)
    # pandaCOO.set_alley_score_weights(access_id, 0.5)
    # pandaCOO.set_scale_parameter(access_id, 10)
    #
    # dance_matrix = [
    #     (0, 1),
    #     (1, 2),
    #     (2, 3),
    #     (3, 4),
    #     (4, 5),
    #     (5, 6),
    #     (6, 7),
    #     (7, 8),
    #     (8, 9),
    #     (9, 10),
    #     (10, 11),
    #     (11, 0),
    # ]
    # pandaCOO.set_dance_matrix(access_id, 0.75, dance_matrix)
    #
    # print(pandaCOO.get_pandas_attribute_score(panda))
    # print(pandaCOO.get_pandas_attribute_score(panda, total=True))
    # print(pandaCOO.get_panda_dance_score(panda))
    # print(sum(pandaCOO.get_panda_dance_score(panda)))


